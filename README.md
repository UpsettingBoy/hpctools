## Compiling
Create `build` folder and inside use `cmake [OPTIONS] ..` with the following optional options:
- Release or debug build: `-DCMAKE_BUILD_TYPE=<type>`
- Custom compiler: `-DCMAKE_C_COMPILER=<cc>`
- Use *MKL instead of OpenBLAS: `-DENABLE_MKL=true`

*: Don't forget to load [Intel enviroment variables](https://www.intel.com/content/www/us/en/develop/documentation/onemkl-macos-developer-guide/top/getting-started/setting-environment-variables.html) for MKL.
