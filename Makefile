LIBS_OBLAS = -lm -lopenblas
LIBS_MKL = -lm -mkl

GCC_FLAGS = -g -march=native -mtune=native -ftree-loop-vectorize -ftree-slp-vectorize -fopenmp
ICC_FLAGS = -g -xHost -parallel -qopt-matmul -qopenmp

icc-executable:
	icc $(ICC_FLAGS) -DENABLE_MKL src/* $(LIBS_MKL) -o bin/dgesv-icc-$(KIND)

gcc-executable:
	gcc $(GCC_FLAGS) src/* $(LIBS_OBLAS) -o bin/dgesv-gcc-$(KIND)

gcc-O0: KIND=O0
gcc-O0: GCC_FLAGS+=-O0
gcc-O0: gcc-executable

gcc-O1: KIND=O1 
gcc-O1: GCC_FLAGS+=-O1
gcc-O1: gcc-executable

gcc-O2: KIND=O2 
gcc-O2: GCC_FLAGS+=-O2
gcc-O2: gcc-executable

gcc-O3: KIND=O3 
gcc-O3: ICC_FLAGS+=-O3
gcc-O3: gcc-executable

gcc-Ofast: KIND=Ofast 
gcc-Ofast: GCC_FLAGS+=-Ofast
gcc-Ofast: gcc-executable

icc-O0: KIND=O0
icc-O1: ICC_FLAGS+=-O0
icc-O0: icc-executable

icc-O1: KIND=O1 
icc-O1: ICC_FLAGS+=-O1
icc-O1: icc-executable

icc-O2: KIND=O2 
icc-O2: ICC_FLAGS+=-O2
icc-O2: icc-executable

icc-O3: KIND=O3 
icc-O3: ICC_FLAGS+=-O3
icc-O3: icc-executable

icc-Ofast: KIND=Ofast 
icc-Ofast: ICC_FLAGS+=-Ofast
icc-Ofast: icc-executable

clean:
	rm -rf ./bin