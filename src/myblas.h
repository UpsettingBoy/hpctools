#ifndef __MYBLAS_H__
#define __MYBLAS_H__

#include <stdint.h>

/// @brief LU decomposition with partial pivoting
/// @param input Input matrix
/// @param L (out) Lower triangular matrix. Pre-allocate to 0s.
/// @param U (out) Upper triangular matrix. Pre-allocate to 0s.
/// @param P (out) Permutation matrix. Pre-allocate to 0s. No output yet.
/// @param size Size of the matrices
/// @return -1 if upper diagonal contains at least a 0.
uint32_t myblas_lu_decomposition(const double *input, double *L, double *U,
                                 int32_t *P, uint32_t size);
uint32_t myblas_lu_decomposition_transposed(const double *input, double *L,
                                            double *U, int32_t *P,
                                            uint32_t size);

/// @brief Solves the system for LX = B
/// @param L (in)  Lower triangular matrix.
/// @param B (in)  Coefficient matrix.
/// @param X (out) Solutions.
/// @param size Size of the matrices
void myblas_forward_substitution(const double *L, const double *B, double *X,
                                 uint32_t size);

/// @brief Solves the system for UX = B
/// @param U (in)  Upper triangular matrix.
/// @param B (in)  Coefficient matrix.
/// @param X (out) Solutions.
/// @param size Size of the matrices
void myblas_backwards_substitution(const double *U, const double *B, double *X,
                                   uint32_t size);

void myblas_mat_mult(const double *A, const double *B, double *C,
                     uint32_t size);

void myblas_print_mat(const double *A, uint32_t n, const char *name);

void myblas_transpose_mat(const double *A, double *At, uint32_t size);

#endif // __MYBLAS_H__
