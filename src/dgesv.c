#include <math.h>
#include <memory.h>
#include <omp.h>
#include <stdio.h>
#include <stdlib.h>
#include <time.h>

#ifdef ENABLE_MKL
#include <mkl_lapacke.h>
#else
#include <openblas/lapacke.h>
#endif

#include "myblas.h"

double *generate_matrix(int size) {
  int i;
  double *matrix = (double *)malloc(sizeof(double) * size * size);

  srand(1);

  for (i = 0; i < size * size; i++) {
    matrix[i] = rand() % 100;
  }

  return matrix;
}

int is_nearly_equal(double x, double y) {
  const double epsilon = 1e-5 /* some small number */;
  return abs(x - y) <= epsilon * abs(x);
  // see Knuth section 4.2.2 pages 217-218
}

int check_result(double *bref, double *b, int size) {
  int i;

  for (i = 0; i < size * size; i++) {
    if (!is_nearly_equal(bref[i], b[i]))
      return 0;
  }

  return 1;
}

int my_dgesv(int n, int nrhs, double *a, int lda, int *ipiv, double *b,
             int ldb) {
  /*
   * Currently known limitations
   * - lda and ldb are not used
   * - No partial pivoting (ipiv not used)
   * - Some allocations. Possibly reuse input parameters
   */

  /*
   * Required for LU decomposition
   * Eventually merge into A (no more allocations)
   */
  double *L = calloc(n * n, sizeof(double));
  double *U = calloc(n * n, sizeof(double));
  double *Ut = calloc(n * n, sizeof(double));

  /*
   * Required for forward and backwards substitution
   * Explore if allocations can be removed from here using input params
   * Eventually use B as temp so no more allocations
   */
  double *Y = calloc(n * n, sizeof(double));
  double *X = calloc(n * n, sizeof(double));

  /*
   * LU decomposition, with no pivoting
   */

  // int err = myblas_lu_decomposition(a, L, U, ipiv, n);
  // if (err != 0) {
  //   exit(err);
  // }

  int err = myblas_lu_decomposition_transposed(a, L, Ut, ipiv, n);
  if (err != 0) {
    exit(err);
  }
  myblas_transpose_mat(Ut, U, n);

  /*
   * Forward and backwards substitution for each B column (rows in B transposed)
   * First:   LY = Bt[col], for Y (forward   substitution)
   * Second:  UX = Y      , for X (backwards substitution)
   * Thrid:   Store X in Bt[col]
   * Profit:  Transpose Bt back to result
   */
  double *Bt = calloc(n * nrhs, sizeof(double)); // Transposed
  myblas_transpose_mat(b, Bt, n);

#pragma omp parallel for
  for (size_t cols = 0; cols < nrhs; cols++) {
    uint32_t offset = cols * n;
    myblas_forward_substitution(L, Bt + (cols * n), Y + offset, n);
    myblas_backwards_substitution(U, Y + offset, X + offset, n);

    // Copy X into Bt row
    memcpy(Bt + (cols * n), X + offset, nrhs * sizeof(double));
  }

  // Transpose result
  myblas_transpose_mat(Bt, b, n);

  // Clean-up
  free(L);
  free(U);
  free(Ut);
  free(X);
  free(Y);
  free(Bt);

  return 0;
}

int main(int argc, char *argv[]) {
  int size = atoi(argv[1]);

  double *a, *aref;
  double *b, *bref;

  a = generate_matrix(size);
  aref = generate_matrix(size);
  b = generate_matrix(size);
  bref = generate_matrix(size);

  // Using LAPACK dgesv OpenBLAS implementation to solve the system
  int n = size, nrhs = size, lda = size, ldb = size, info;
  int *ipiv = (int *)malloc(sizeof(int) * size);

  double tStart = omp_get_wtime();
  info = LAPACKE_dgesv(LAPACK_ROW_MAJOR, n, nrhs, aref, lda, ipiv, bref, ldb);
  printf("Time taken by LAPACKE: %.2fs\n", omp_get_wtime() - tStart);

  int *ipiv2 = (int *)malloc(sizeof(int) * size);

  tStart = omp_get_wtime();
  my_dgesv(n, nrhs, a, lda, ipiv2, b, ldb);
  printf("Time taken by my implementation: %.2fs\n", omp_get_wtime() - tStart);

  // Result printing
  // myblas_print_mat(bref, nrhs, "Reference result");
  // myblas_print_mat(b, n, "My implementation");

  if (check_result(bref, b, size) == 1)
    printf("Result is ok!\n");
  else
    printf("Result is wrong!\n");

  // Clean-up
  free(a);
  free(aref);
  free(b);
  free(bref);
  free(ipiv);
  free(ipiv2);

  return 0;
}
