#include "myblas.h"

#include <omp.h>
#include <stdio.h>
#include <stdlib.h>

void myblas_mat_mult(const double *A, const double *B, double *C,
                     uint32_t size) {
  for (size_t i = 0; i < size; i++) {
    for (size_t j = 0; j < size; j++) {
      for (size_t k = 0; k < size; k++) {
        C[i * size + j] += A[i * size + k] * B[k * size + j];
      }
    }
  }
}

uint32_t myblas_lu_decomposition(const double *input, double *L, double *U,
                                 int32_t *P, uint32_t size) {
  /*
   *  L is unit lower triangular matrix (diagonal is 1)
   *  U is upper triangular matrix
   *  P is permutation matrix (not used yet)
   */

  // First row processing
  uint32_t error = 0;
  for (size_t i = 0; i < size; i++) {
    // Set unit diagonal on lower triangular
    L[i * size + i] = 1.0;

#pragma omp parallel for schedule(dynamic)
    for (size_t j = i; j < size; j++) {
      // Dot product row/columns upper and lower
      double dot = 0.0;
#pragma code_align 32
      for (size_t k = 0; k < size - 1; k++) {
        dot += L[i * size + k] * U[k * size + j];
      }

      U[i * size + j] = input[i * size + j] - dot;
    }

#pragma code_align 32
#pragma omp parallel for schedule(dynamic)
    for (size_t j = i + 1; j < size; j++) {
      double dot = 0.0;
#pragma code_align 32
      for (size_t k = 0; k < size - 1; k++) {
        dot += L[j * size + k] * U[k * size + i];
      }

      // If upper triangular diagonal is 0 cannot continue, cannot divide by 0
      if (U[i * size + i] == 0.0) {
#pragma omp atomic write
        error = -1;
      } else {
        L[j * size + i] = (input[j * size + i] - dot) / U[i * size + i];
      }
    }
  }

  return error;
}

uint32_t myblas_lu_decomposition_transposed(const double *input, double *L,
                                            double *U, int32_t *P,
                                            uint32_t size) {
  /*
   *  L is unit lower triangular matrix (diagonal is 1)
   *  U is upper triangular matrix, but transposed, so memory acceses are now
   *    mostly lineal
   *  P is permutation matrix (not used yet)
   */

  // First row processing
  uint32_t error = 0;
  for (size_t i = 0; i < size; i++) {
    // Set unit diagonal on lower triangular
    L[i * size + i] = 1.0;

#pragma omp parallel for schedule(dynamic)
    for (size_t j = i; j < size; j++) {
      // Dot product row/columns upper and lower
      double dot = 0.0;
#pragma code_align 32
      for (size_t k = 0; k < size - 1; k++) {
        dot += L[i * size + k] * U[j * size + k];
      }

      U[j * size + i] = input[i * size + j] - dot;
    }

#pragma code_align 32
#pragma omp parallel for schedule(dynamic)
    for (size_t j = i + 1; j < size; j++) {
      double dot = 0.0;
#pragma code_align 32
      for (size_t k = 0; k < size - 1; k++) {
        dot += L[j * size + k] * U[i * size + k];
      }

      // If upper triangular diagonal is 0 cannot continue, cannot divide by 0
      if (U[i * size + i] == 0.0) {
#pragma omp atomic write
        error = -1;
      } else {
        L[j * size + i] = (input[j * size + i] - dot) / U[i * size + i];
      }
    }
  }

  return error;
}

void myblas_forward_substitution(const double *L, const double *B, double *X,
                                 uint32_t size) {
  for (size_t i = 0; i < size; i++) {
    double dot = 0.0;
    for (size_t j = 0; j < i; j++) {
      dot += L[i * size + j] * X[j];
    }

    X[i] = (B[i] - dot) / L[i * size + i];
  }
}

void myblas_backwards_substitution(const double *U, const double *B, double *X,
                                   uint32_t size) {
  for (int32_t i = size - 1; i >= 0; --i) {
    double dot = 0.0;
    for (size_t j = i + 1; j < size; j++) {
      dot += U[i * size + j] * X[j];
    }

    X[i] = (B[i] - dot) / U[i * size + i];
  }
}

void myblas_print_mat(const double *A, uint32_t n, const char *name) {
  printf("%s\n", name);
  for (size_t i = 0; i < n; i++) {
    for (size_t j = 0; j < n; j++) {
      printf("%10.3f, ", A[i * n + j]);
    }
    printf("\n");
  }
  printf("\n");
}

void myblas_transpose_mat(const double *A, double *At, uint32_t size) {
  uint32_t blocksize = size >> 5; 
  for (int i = 0; i < size; i += blocksize) {
    for (int j = 0; j < size; j += blocksize) {
      for (int k = i; k < i + blocksize; ++k) {
        for (int l = j; l < j + blocksize; ++l) {
          At[l * size + k] = A[k * size + l];
        }
      }
    }
  }
}